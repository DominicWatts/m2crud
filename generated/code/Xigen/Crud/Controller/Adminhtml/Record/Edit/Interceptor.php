<?php
namespace Xigen\Crud\Controller\Adminhtml\Record\Edit;

/**
 * Interceptor class for @see \Xigen\Crud\Controller\Adminhtml\Record\Edit
 */
class Interceptor extends \Xigen\Crud\Controller\Adminhtml\Record\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Xigen\Crud\Model\RecordFactory $modelRecordFactory)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $resultPageFactory, $modelRecordFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
