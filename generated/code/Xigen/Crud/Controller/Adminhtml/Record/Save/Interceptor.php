<?php
namespace Xigen\Crud\Controller\Adminhtml\Record\Save;

/**
 * Interceptor class for @see \Xigen\Crud\Controller\Adminhtml\Record\Save
 */
class Interceptor extends \Xigen\Crud\Controller\Adminhtml\Record\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, \Xigen\Crud\Model\RecordFactory $modelRecordFactory)
    {
        $this->___init();
        parent::__construct($context, $dataPersistor, $modelRecordFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
