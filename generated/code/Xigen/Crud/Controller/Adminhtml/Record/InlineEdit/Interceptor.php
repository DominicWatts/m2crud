<?php
namespace Xigen\Crud\Controller\Adminhtml\Record\InlineEdit;

/**
 * Interceptor class for @see \Xigen\Crud\Controller\Adminhtml\Record\InlineEdit
 */
class Interceptor extends \Xigen\Crud\Controller\Adminhtml\Record\InlineEdit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $jsonFactory, \Xigen\Crud\Model\RecordFactory $modelRecordFactory)
    {
        $this->___init();
        parent::__construct($context, $jsonFactory, $modelRecordFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
