<?php
namespace Xigen\Crud\Controller\Adminhtml\Record\Delete;

/**
 * Interceptor class for @see \Xigen\Crud\Controller\Adminhtml\Record\Delete
 */
class Interceptor extends \Xigen\Crud\Controller\Adminhtml\Record\Delete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Xigen\Crud\Model\RecordFactory $modelRecordFactory)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $modelRecordFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
