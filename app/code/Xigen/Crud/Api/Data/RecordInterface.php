<?php
/**
 * Xigen Magento 2 Crud demo extension
 * Copyright (C) 2017  2017
 *
 * This file is part of Xigen/Crud.
 *
 * Xigen/Crud is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xigen\Crud\Api\Data;

interface RecordInterface
{
    const ID = 'id';
    const CONTENT = 'content';
    const DATE_CREATED = 'date_created';
    const DATE_UPDATED = 'date_updated';

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setId($id);

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     * @param string $content
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setContent($content);

    /**
     * Get date_created
     * @return string|null
     */
    public function getDateCreated();

    /**
     * Set date_created
     * @param string $date_created
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setDateCreated($date_created);

    /**
     * Get date_updated
     * @return string|null
     */
    public function getDateUpdated();
    
    /**
     * Set date_updated
     * @param string $date_updated
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setDateUpdated($date_updated);
}
