<?php
/**
 * Xigen Magento 2 Crud demo extension
 * Copyright (C) 2017  2017
 *
 * This file is part of Xigen/Crud.
 *
 * Xigen/Crud is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xigen\Crud\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RecordRepositoryInterface
{


    /**
     * Save Record
     * @param \Xigen\Crud\Api\Data\RecordInterface $record
     * @return \Xigen\Crud\Api\Data\RecordInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Xigen\Crud\Api\Data\RecordInterface $record
    );

    /**
     * Retrieve Record
     * @param string $recordId
     * @return \Xigen\Crud\Api\Data\RecordInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($recordId);

    /**
     * Retrieve Record matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Xigen\Crud\Api\Data\RecordSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Record
     * @param \Xigen\Crud\Api\Data\RecordInterface $record
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Xigen\Crud\Api\Data\RecordInterface $record
    );

    /**
     * Delete Record by ID
     * @param string $recordId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($recordId);
}
