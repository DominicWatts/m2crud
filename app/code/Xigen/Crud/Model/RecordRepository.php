<?php
/**
 * Xigen Magento 2 Crud demo extension
 * Copyright (C) 2017  2017
 *
 * This file is part of Xigen/Crud.
 *
 * Xigen/Crud is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xigen\Crud\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SortOrder;
use Xigen\Crud\Api\Data\RecordSearchResultsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Store\Model\StoreManagerInterface;
use Xigen\Crud\Model\ResourceModel\Record as ResourceRecord;
use Xigen\Crud\Api\RecordRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Xigen\Crud\Api\Data\RecordInterfaceFactory;
use Xigen\Crud\Model\ResourceModel\Record\CollectionFactory as RecordCollectionFactory;

class RecordRepository implements recordRepositoryInterface
{
    protected $resource;

    protected $dataObjectProcessor;

    private $storeManager;

    protected $recordFactory;

    protected $recordCollectionFactory;

    protected $dataRecordFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;


    /**
     * @param ResourceRecord $resource
     * @param RecordFactory $recordFactory
     * @param RecordInterfaceFactory $dataRecordFactory
     * @param RecordCollectionFactory $recordCollectionFactory
     * @param RecordSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceRecord $resource,
        RecordFactory $recordFactory,
        RecordInterfaceFactory $dataRecordFactory,
        RecordCollectionFactory $recordCollectionFactory,
        RecordSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->recordFactory = $recordFactory;
        $this->recordCollectionFactory = $recordCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRecordFactory = $dataRecordFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Xigen\Crud\Api\Data\RecordInterface $record
    ) {
        /* if (empty($record->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $record->setStoreId($storeId);
        } */
        try {
            $record->getResource()->save($record);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the record: %1',
                $exception->getMessage()
            ));
        }
        return $record;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($recordId)
    {
        $record = $this->recordFactory->create();
        $record->getResource()->load($record, $recordId);
        if (!$record->getId()) {
            throw new NoSuchEntityException(__('Record with id "%1" does not exist.', $recordId));
        }
        return $record;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->recordCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Xigen\Crud\Api\Data\RecordInterface $record
    ) {
        try {
            $record->getResource()->delete($record);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Record: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($recordId)
    {
        return $this->delete($this->getById($recordId));
    }
}
