<?php
/**
 * Xigen Magento 2 Crud demo extension
 * Copyright (C) 2017  2017
 *
 * This file is part of Xigen/Crud.
 *
 * Xigen/Crud is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xigen\Crud\Model;

use Xigen\Crud\Api\Data\RecordInterface;

class Record extends \Magento\Framework\Model\AbstractModel implements RecordInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Xigen\Crud\Model\ResourceModel\Record');
    }

    /**
     * Get id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get content
     * @return string
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Set content
     * @param string $content
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get date_created
     * @return string
     */
    public function getDateCreated()
    {
        return $this->getData(self::DATE_CREATED);
    }

    /**
     * Set date_created
     * @param string $date_created
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setDateCreated($date_created)
    {
        return $this->setData(self::DATE_CREATED, $date_created);
    }

    /**
     * Get date_updated
     * @return string
     */
    public function getDateUpdated()
    {
        return $this->getData(self::DATE_UPDATED);
    }

    /**
     * Set date_updated
     * @param string $date_updated
     * @return \Xigen\Crud\Api\Data\RecordInterface
     */
    public function setDateUpdated($date_updated)
    {
        return $this->setData(self::DATE_UPDATED, $date_updated);
    }

    /*
    protected function _beforeSave($object) {
        parent::_beforeSave($object);
        $object->setData('updated_at', date('Y-m-d H:i:s'));
        return $this;
    }
    */
}
