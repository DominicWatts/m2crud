<?php
/**
 * Xigen Magento 2 Crud demo extension
 * Copyright (C) 2017  2017
 *
 * This file is part of Xigen/Crud.
 *
 * Xigen/Crud is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xigen\Crud\Model\ResourceModel;

class Record extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Date model
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * constructor
     *
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        $this->_date = $date;
        parent::__construct($context);
    }

    /**
     * Define resource model
     *
     * @return void
     */
    
    protected function _construct()
    {
        $this->_init('xigen_crud_record', 'id');
    }

    /**
     * before save callback
     *
     * @param \Magento\Framework\Model\AbstractModel
     */
    /*
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        // $object->setUpdatedAt($this->_date->date());
        // if ($object->isObjectNew()) {
            $object->setUpdatedAt($this->_date->date());
        // }
        return parent::_beforeSave($object);
    }
    */
}
