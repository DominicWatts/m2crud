<?php
/**
 * Xigen Magento 2 Crud demo extension
 * Copyright (C) 2017  2017
 *
 * This file is part of Xigen/Crud.
 *
 * Xigen/Crud is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xigen\Crud\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Create extends Command
{

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var \Xigen\Crud\Model\RecordFactory
     */
    protected $modelRecordFactory;

    /**
     * @param \Xigen\Crud\Model\RecordFactory $modelRecordFactory
     * @param \Magento\Framework\App\State $appState
     */
    public function __construct(
        \Xigen\Crud\Model\RecordFactory $modelRecordFactory,
        \Magento\Framework\App\State $appState
    ) {
        parent::__construct();
        $this->modelRecordFactory = $modelRecordFactory;
        $this->appState = $appState;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $recordModel = $this->modelRecordFactory->create();
        $recordModel->setContent('test content');
        $recordModel->setDateUpdated(date('Y-m-d H:i:s'));
        $recordModel->setDateCreated(date('Y-m-d H:i:s'));
        try {
            $recordModel->save();
            $output->writeln('Item id ' . $recordModel->getId() . ' item saved');
        } catch (\Exception $e) {
            $output->writeln('Something went wrong while saving the Record.');
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("xigen_crud:create");
        $this->setDescription("Create demo action");
        parent::configure();
    }
}
